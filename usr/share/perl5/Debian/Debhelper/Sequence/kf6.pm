{
    package Debian::Debhelper::Sequence::kf6;
    use Debian::Debhelper::Dh_Version;
    use Debian::Debhelper::Dh_Lib qw(error);

    sub ensure_debhelper_version {
        my @v = split(/\./, $Debian::Debhelper::Dh_Version::version);
        if ($v[0] > $_[0]) {
            return 1;
        }
        elsif ($v[0] == $_[0]) {
            if ($v[1] > $_[1]) {
                return 1;
            }
            elsif ($v[1] == $_[1]) {
                return $1 >= $_[2] if ($v[2] =~ /^(\d+)/);
            }
        }
        return 0;
    }
    unless (ensure_debhelper_version(7, 3, 16)) {
        error "debhelper addon 'kf6' requires debhelper 7.3.16 or later";
    }

    1;
}

# Build with kf5 buildsystem by default
add_command_options("dh_auto_configure", "--buildsystem=kf6");
add_command_options("dh_auto_build", "--buildsystem=kf6");
add_command_options("dh_auto_test", "--buildsystem=kf6");
add_command_options("dh_auto_install", "--buildsystem=kf6");
add_command_options("dh_auto_clean", "--buildsystem=kf6");

# Exclude kf5 documentation from dh_compress by default
add_command_options("dh_compress",
    qw(-X.dcl -X.docbook -X-license -X.tag -X.sty -X.el));

insert_after("dh_install", "dh_movelibkdeinit");

#Execute dh_missing --list-missing between marks recognized by Kubuntu's
#CI and status reports
insert_after("dh_builddeb","dh_kubuntu_list-missing");

#Execute lintian once the package is built
insert_after("dh_kubuntu_list-missing", "dh_kubuntu_execute_lintian");

1;
